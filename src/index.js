import ClickAlert from "./js/ClickAlert";

const loadWith = (loadEvent = "DOMContentLoaded") => {
  ClickAlert(loadEvent);
};

export default {
  loadWith
};
