import alertOnClick from "./clickAlert";

const init = loader => {
  document.addEventListener(loader, () => {
    const buttons = document.querySelectorAll("button");

    buttons.forEach(button => alertOnClick(button));
  });
};

export default init;
