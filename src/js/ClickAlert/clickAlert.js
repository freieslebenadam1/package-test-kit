
const alertOnClick = (element, message = "You clicked", log = false) => {
  if (!element) return;

  element.addEventListener("click", () => {
    log && (console.log(message));

    alert(message);
  });
};

export default alertOnClick;
